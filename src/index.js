import "dotenv/config.js";
import { ProjectApi } from "./services/projectApi.service.js";
import { Component } from "./services/component.service.js";

function printComponents(components) {
  components
    .sort((a, b) => (a.id > b.id && 1) || -1)
    .forEach((item) => {
      console.log("\n");
      console.log(
        `Component: id: ${item.id}, ${item.name}, issues: ${item.issues.length}`
      );
      console.table(item.issues);
    });
}

(async function () {
  try {
    const project = new ProjectApi("IC");
    const component = new Component(project);

    const componentsIssues = await component.getNoLeadComponentsIssues();

    printComponents(componentsIssues);
  } catch (error) {
    console.error("Error in fetchihg components: ", error);
  }
})();
