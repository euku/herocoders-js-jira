import axios from "axios";

export const ISSUES_FETCH_BUCKET_DEFAULT = 50;

export class ProjectApi {
  api;
  project;

  constructor(projectName) {
    this.project = projectName;
    this.api = axios.create({
      baseURL: process.env.API,
    });
  }

  async getComponents() {
    const resp = await this.api.get(`/project/${this.project}/components`);
    return resp.data;
  }

  async getIssues(startAt = 0, maxResults = ISSUES_FETCH_BUCKET_DEFAULT) {
    const resp = await this.api.get(
      `/search?jql=project=${this.project}&startAt=${startAt}&maxResults=${maxResults}`
    );
    return resp.data.issues;
  }
}
