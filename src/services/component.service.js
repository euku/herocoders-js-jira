const LEAD_TYPE = "COMPONENT_LEAD";

export class Component {
  api;

  constructor(projectApi) {
    this.api = projectApi;
  }

  componentHasLead = (component) =>
    component.hasOwnProperty("lead") || component.type === LEAD_TYPE;

  issueBelongsToComponent = (issue, component) =>
    issue.fields?.components?.find((c) => c.id === component.id) ?? false;

  setComponentsIssues = async (components) => {
    let issues = await this.api.getIssues(0);
    let issueStartIndex = issues.length;

    while (issues.length > 0) {
      components.forEach((component) => {
        const componentIssues = issues
          .filter((issue) => this.issueBelongsToComponent(issue, component))
          .map((issue) => ({
            id: issue.id,
            key: issue.key,
          }));

        component.issues = component.issues.concat(componentIssues);
      });

      issues = await this.api.getIssues(issueStartIndex);
      issueStartIndex += issues.length;
    }
  };

  getNoLeadComponents = async () => {
    const components = await this.api.getComponents();
    return components
      .filter((component) => this.componentHasLead(component) === false)
      .map((component) => ({
        id: component.id,
        name: component.name,
        issues: [],
      }));
  };

  async getNoLeadComponentsIssues() {
    const components = await this.getNoLeadComponents();
    await this.setComponentsIssues(components);
    return components;
  }
}
