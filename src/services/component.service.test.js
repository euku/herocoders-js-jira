import { describe, it, jest } from "@jest/globals";
import { ProjectApi } from "./projectApi.service";
import { Component } from "./component.service";

describe("Component service", () => {
  const project = new ProjectApi("test");
  const component = new Component(project);

  it("Should assign issues to components correctly", async () => {
    jest.spyOn(project, "getComponents").mockResolvedValue([
      {
        id: "1",
        name: "Infrastructure",
      },
      {
        id: "2",
        name: "Marketplace",
      },
      {
        id: "3",
        name: "OpenID",
      },
    ]);

    jest
      .spyOn(project, "getIssues")
      .mockResolvedValue([])
      .mockResolvedValueOnce([
        {
          id: "1",
          key: "IC-1111",
          fields: {
            components: [
              {
                id: "1",
              },
              {
                id: "3",
              },
            ],
          },
        },
        {
          id: "2",
          key: "IC-2222",
          fields: {
            components: [
              {
                id: "2",
              },
            ],
          },
        },
      ])
      .mockResolvedValueOnce([
        {
          id: "3",
          key: "IC-3333",
          fields: {
            components: [
              {
                id: "2",
              },
            ],
          },
        },
        {
          id: "4",
          key: "IC-4444",
          fields: {
            components: [
              {
                id: "1",
              },
            ],
          },
        },
      ]);

    const componentsIssues = await component.getNoLeadComponentsIssues();

    expect(componentsIssues).toEqual([
      {
        id: "1",
        name: "Infrastructure",
        issues: [
          {
            id: "1",
            key: "IC-1111",
          },
          {
            id: "4",
            key: "IC-4444",
          },
        ],
      },
      {
        id: "2",
        name: "Marketplace",
        issues: [
          {
            id: "2",
            key: "IC-2222",
          },
          {
            id: "3",
            key: "IC-3333",
          },
        ],
      },
      {
        id: "3",
        name: "OpenID",
        issues: [
          {
            id: "1",
            key: "IC-1111",
          },
        ],
      },
    ]);
  });
  it("Should find components with no lead only", async () => {
    jest.spyOn(project, "getComponents").mockResolvedValue([
      {
        id: "1",
        name: "Infrastructure",
      },
      {
        id: "2",
        name: "Marketplace",
        lead: {},
      },
      {
        id: "3",
        name: "OpenID",
      },
      {
        id: "4",
        name: "Data analysis",
        assigneeType: "PROJECT_OWNER",
      },
    ]);

    jest.spyOn(project, "getIssues").mockResolvedValue([]);

    const noLeadComponents = await component.getNoLeadComponentsIssues();

    expect(noLeadComponents).toEqual([
      {
        id: "1",
        name: "Infrastructure",
        issues: [],
      },
      {
        id: "3",
        name: "OpenID",
        issues: [],
      },
      {
        id: "4",
        name: "Data analysis",
        issues: [],
      },
    ]);
  });
});
